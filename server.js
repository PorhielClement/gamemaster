/* import des libraires pour le serveur */

const express   = require('express');               // pour créer un serveur web modulable avec Express.js
const app       = express();                        // instanciation du routeur principal
const session   = require('express-session');       // middleware de construction automatique de session
const morgan    = require('morgan');                // middleware de log des requêtes entrant
const passport  = require('passport');              // middleware de gestion des automatique de l'authentification
require('./config/passport')(passport);    // configuration de passport
const flash     = require('connect-flash');         // middleware d'envoi de messages à usage unique

/* configuration du serveur */

app.locals = {'appname':'Game Master','rootPath':__dirname}; // variables globales pour les vues

app.use(express.urlencoded({ extended: false }))    // formatage en entrée des requêtes et paramètres (get et post) : string -> objet
    .use(express.json())                            // formatage en sortie : objet -> string
    .use(session({                                  // paramètres de sauvegarde et de sécurisation des variables et cookies de session
        secret: 'bibidibobidiboo',                  // chaîne d'encryptage
        resave:false,                               // ne pas sauvegarder si session inchangée
        saveUninitialized:true                      // sauvegarder si nouvelle session
    }))
    .use(morgan('dev'))                             // affichage explicite des requêtes
    .use(passport.initialize())                     // initialisation de l'outil d'authentification
    .use(passport.session())                        // établissement du lien entre l'authentification et la session
    .use(express.static(__dirname +'/public'))      // établissement de la racine vers les ressources statiques (css, js, images...)
    .use(flash())                                   // initilisation des messages à usage unique
    .use(function (req, res, next) {                // middleware d'envoi automatique de l'utilisateur authentifié vers toutes les vues
        var user = req.user;
        res.locals = Object.assign(res.locals, { user : user });  // assigniation de l'objet dans les variables locales du routeur
        // (valables pour la requête en cours uniquement)
        next();                                                                 // passage au prochain middleware
    }).set('views','./views/')                                                  // racine des vues
    .set('view engine','ejs')                                                   // moteur de vues EJS

/* configuration des routeurs (segmentation des fonctionnalités) */
const public = express.Router()                                 // routeur pour les pages principales (sans authentification)
require('./routes/public')(public)                       // passage du routeur dans la fonction
app.use('/', public)                                            // assignation du routeur comme un middleware

const auth = express.Router()                                   // routeur pour l'authentification (/auth/*)
require('./routes/auth')(auth,passport)
app.use('/auth', auth)


const secure = express.Router()                                 // routeur pour les pages accessible seulement si authentifié
require('./routes/secure')(secure)
app.use('/', secure)


app.listen(80, () => {                                          // lancement du serveur sur le port HTTP (80)
    console.log('Listening well !')
});

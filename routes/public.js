module.exports = (router) => {                  // affichage de la page d'accueil pour deux routes (/ et /index)
    router.get(['/','/index'], (req, res) =>
        res.render('index')
    );
}

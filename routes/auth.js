/* import de fonctions utiles */
const db = require('../model/lowdb/');  // fonctions pour la base de donnée (ici, lowdb)
const daoUser = new db.DaoUser();       // fonction DAO pour la table User


module.exports = (router,passport) => {
    router.get('/login', (req, res) => {        // pour la route /auth/login
        if(req.user) res.redirect('/board')     // rediriger vers le profil si déjà authentifié
        // sinon, affichage la page de connexion, envoi d'erreur/succès s'il y en a (message à usage unique)
        else res.render('connexionform',{errorMessage:req.flash('errorMessage')[0],successMessage:req.flash('successMessage')[0]})
    }).get('/signin', (req, res) => {
        if(req.user) res.redirect('/board')
        else res.render('registerform',{errorMessage:req.flash('errorMessage')[0],successMessage:req.flash('successMessage')[0]});
    }).get('/logout', (req, res) => {
        req.logout();                           // utilisation de la fonction de logout rajoutée par passport
        res.redirect('/index')                  // redirection vers la page d'accueil
    }).post('/register',passport.authenticate('local-signin',{  // utilisation de la stratégie locale de connexion
        successRedirect:'/board',                               // redirection vers le profil en cas de succès
        failureRedirect:'/auth/signin'                          // bouclage (avec erreur) en cas d'échec
    })).post('/checkLogin',passport.authenticate('local-login',{// stratégie locale d'enregistrement
        successRedirect:'/board',
        failureRedirect:'/auth/login'
    })).get('/github',
        passport.authenticate('github')                         // stratégie de connexion/enregistrement via GitHub
    ).get('/github/callback',
        passport.authenticate(
            'github', {
                successRedirect: '/board',
                failureRedirect: '/auth/login',
            }
        ),
    ).get('/', (req, res) => {                                  // si le chemin est /auth/
        if(req.user) res.redirect('/board')                     // redirection vers le profil si déjà authentifié
        else res.render('auth')                                 // affichage des options de connexion
    });
};

const express = require('express'); // import pour création d'un nouveau routeur

module.exports = (router) => {
    router.use((req, res, next) => {        // middleware de filtrage d'authnetification
        if (req.isAuthenticated())          // si l'utilisateur est authentifié (fonction de passport)
            next();                         // passage au prochain middleware
        else res.redirect('/auth')          // sinon redirection vers la page de connexion
    });

    const game = express.Router();          // création du routeur game
    require('./game')(game);
    router.use('/game', game);

    router.get('/board', (req, res) => {    // page pour le profil
        res.render('board', {errorMessage:req.flash('errorMessage')[0],successMessage:req.flash('successMessage')[0]})
    });
};

const db = require('../model/lowdb/');
const daoGame = new db.DaoGame();
const daoScore = new db.DaoScore();

module.exports = (router) => {
    router.get('/scores', (req, res) => {
        daoScore.getAllScores()
            .then((list) =>
                res.render('globalScores',{scores:list.reverse()})
            ).catch((err) => {
            console.log(err);
            res.render('globalScores',{err:err})
        })
    }).get('/:userLogin/scores', (req, res) => {
        daoScore.getScoresByUser(req.params.userLogin)
            .then((list) =>
                res.render('scores',{scores:list})
            ).catch((err) => {
            console.log(err);
            res.render('scores',{err:err})
        })
    }).get('/:game', (req, res) => {
        res.render('games/' + req.params.game);
    }).get('/', (req, res) =>{
        daoGame.getAllGames()
            .then((games)=> {
                console.log(1, games);
                daoScore.getScoresByUser(req.user._login)
                    .then((scores) => {
                        console.log(2, scores);
                        const list = games.map((game) => {
                            const score = scores.find((score) => game._access === score._gameId);
                            if(score && score._points) game._points = score._points;
                            return game;
                        })
                        res.render('gameList', {list: list})
                    })

            }).catch((err) => {
            console.log(err);
            res.render('gameList', {err:err})
        })
    }).post('/:gameId', (req, res) => {
        const _userLogin    = req.body.userLogin;
        const _gameId       = req.params.gameId;
        const _points       = req.body.score;
        daoScore.getScoreByUserByGame(_userLogin, _gameId)
            .then((result) => {
                if(result) {
                    if (result._points < _points) {
                        daoScore.updateScore(result,{_userLogin, _gameId, _points})
                    }
                } else {
                    daoScore.createScore(         {_userLogin, _gameId, _points})
                }
                res.end()
            }).catch((err) => {
            console.log(err);
            res.end();
        })
    })
}
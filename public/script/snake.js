// à l'ouverture de la fenètre du navigateur
// couleur pomme B21919
window.onload = function () {
	//variables globales
	const canvasWidth = 900;
	const canvasHeight = 600;
	const cellSize = 20;
	const cellWidth = canvasWidth / cellSize;
	const cellHeight = canvasHeight / cellSize;
	let ctx;
	let snaky;
	let apple;
	let timeOut;
	let delay;
	let canvas = document.querySelector("canvas");
	let score = 0;
	let maxScore = 0;
	const colors = ["#9400D3", "#4B0082", "#0000FF", "#00FF00", "#FFFF00", "#FF7F00", "#FF0000"];
	let snekColorIndex = 4;
	let appleColorIndex = 0;
	const canvasColors = ["rgba(0,0,0,0.5)", "rgba(50,50,50,0.5)", "rgba(100,100,100,0.5)", "rgba(150,150,150,0.5)", "rgba(200,200,200,0.5)", "rgba(255,255,255,0.5)", "rgba(200,200,200,0.5)", "rgba(150,150,150,0.5)", "rgba(100,100,100,0.5)", "rgba(50,50,50,0.5)"];
	let canvasColorIndex = 0;
	let HardcoreModeToggle = false; //vaut true si activé

	class Snek {
		constructor(body, direction) {
			this.body = body;
			this.direction = direction;
		}

		draw(){
			ctx.save();
			ctx.fillStyle = "#0F0";
			if(HardcoreModeToggle){ctx.fillStyle = colors[snekColorIndex];}
			for (let i = 0; i<this.body.length; i++){
				drawBlock(ctx,this.body[i]);
			}
			ctx.restore();
		}

		drawDead(){
			ctx.save();
			ctx.fillStyle = "#040";
			for (let i = 0; i<this.body.length; i++){
				drawBlock(ctx,this.body[i]);
			}
			ctx.restore();
		}

		move(){
			const nextPosition = this.body[0].slice();
			switch(this.direction){
				case "left" : nextPosition[0] --; break;
				case "right" : nextPosition[0] ++; break;
				case "down" : nextPosition[1] ++; break;
				case "up" : nextPosition[1] --; break;
				default : throw("invalid direction in move");
			}
			this.body.unshift(nextPosition);
			if(!this.ateApple){
				this.body.pop();
			}else {
				this.ateApple = false;
			}
		}

		setDirection(newDirection){
			let allowedDirections;
			switch(this.direction){
				case "left": allowedDirections=["up","down"]; break;
				case "right": allowedDirections=["up","down"]; break;
				case "down": allowedDirections=["left","right"]; break;
				case "up": allowedDirections=["left","right"]; break;
				default : throw("invalid direction in allowed direction");
			}
			if(allowedDirections.indexOf(newDirection) > -1){
				this.direction = newDirection;
			}
		}

		checkCollision() {
			let wallCollision = false;
			let snakeCollision = false;
			const head = this.body[0];
			const rest = this.body.slice(1);
			const snakeX = head[0];
			const snakeY = head[1];
			const minX = 0;
			const maxX = cellWidth - 1;
			const minY = 0;
			const maxY = cellHeight - 1;
			const isNotBetweenHorizontalWalls = snakeX < minX || snakeX > maxX;
			const isNotBetweenVerticalWalls = snakeY < minY || snakeY > maxY;

			if (isNotBetweenHorizontalWalls || isNotBetweenVerticalWalls) {
				wallCollision = true;
			}

			for(let i=0; i < rest.length; i++){
				if (snakeX === rest[i][0] && snakeY === rest[i][1]){
					snakeCollision = true;
				}
			}

			return wallCollision || snakeCollision;
		}

		isEatingApple(appleToEat) {
			const head = this.body[0];
			if(head[0]===appleToEat.position[0] && head[1]===appleToEat.position[1]){
				delay = delay * 0.95;
				score ++;
				//var audio = new Audio('./crunch.mp3');
				//audio.play();
			}
			return (head[0]===appleToEat.position[0] && head[1]===appleToEat.position[1]);
		}
	}

	class Apple {
		position;

		constructor(position) {
			this.position = position;
		}

		drawApple(){
			ctx.save();
			ctx.fillStyle = "#F00";
			if(HardcoreModeToggle){ctx.fillStyle = colors[appleColorIndex];}
			ctx.beginPath();
			const radius = cellSize / 2;
			const x = this.position[0] * cellSize + radius;
			const y = this.position[1] * cellSize + radius;
			ctx.arc(x, y, radius, 0, Math.PI*2, true);
			ctx.fill();
			ctx.restore();
		}

		setNewPosition(){
			const newX = Math.round(Math.random() * (canvasWidth / cellSize - 1));
			const newY = Math.round(Math.random() * (canvasHeight / cellSize - 1));
			console.log(newX.toString());
			console.log(newY.toString());
			this.position = [newX,newY];
		};

		isOnSnake(snakeToCheck) {
			let isOnSnake = false;
			for(let i = 0; i< snakeToCheck.body.length; i++) {
				if(this.position[0] === snakeToCheck.body[i][0] && this.position[1] === snakeToCheck.body[i][1]) {
					isOnSnake = true;
				}
			}
			return isOnSnake;
		}

	}

	function restart(){
		snaky = new Snek([[14,4],[13,4],[12,4],[11,4],[10,4],[9,4],[8,4],[7,4],[6,4],[5,4],[4,4],[3,4],[2,4]],"right");
		apple = new Apple([20,14]);
		delay = 50;
		score = 0;
		HardcoreModeToggle = 0;
		clearTimeout(timeOut);
		refreshCanvas();
	}

	function init() {
		if (canvas === undefined) {
			canvas = document.createElement('canvas');
			canvas.width = canvasWidth;
			canvas.height = canvasHeight;
			canvas.style.border = "2px solid #0F0";
			canvas.style.display = "block";
			canvas.style.backgroundColor = "rgba(0,255,0,0.1)";
			document.body.appendChild(canvas);
			document.getElementById("game").className = "noClass";
		}
		ctx = canvas.getContext('2d');
		restart();
	}

	function drawBlock(ctx,position){	//dessin des blocs du serpent
		const x = position[0] * cellSize;
		const y = position[1] * cellSize;
		ctx.fillRect(x,y,cellSize,cellSize);
	}

	function NewColor(){
		snekColorIndex ++;
		if(snekColorIndex > 6){snekColorIndex = 0;}
		appleColorIndex ++;
		if(appleColorIndex > 6){appleColorIndex = 0;}
	}

	function CanvasStrobo(){
		canvasColorIndex ++;
		if(canvasColorIndex > 9){canvasColorIndex = 0;}
		canvas.style.backgroundColor = canvasColors[canvasColorIndex];
	}

	function drawScore(){
		ctx.save();
		const centerX = canvasWidth / 2;
		const centerY = canvasHeight / 2;
		ctx.fillStyle = "#050";
		if(HardcoreModeToggle){
			ctx.fillStyle = colors[appleColorIndex];
		}
		ctx.textAlign = "center";
		ctx.font = "100px 'Press Start 2P'";
		ctx.fillText(score, centerX, centerY+200);
		ctx.restore();
	}

	function gameOver() {
		canvas.style.backgroundColor = "rgba(0,255,0,0.1)";
		ctx.save();
		ctx.clearRect(0,0,canvasWidth,canvasHeight);
		snaky.drawDead();
		ctx.font = "90px 'Press Start 2P'";
		ctx.fillStyle = "#0F0";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.strokeStyle = "#0F0";
		ctx.lineWidth = 5;
		const centerX = canvasWidth / 2;
		const centerY = canvasHeight / 2;
		ctx.fillText("Game Over", centerX, centerY-50);
		ctx.font = "20px 'Press Start 2P'";
		ctx.fillText("Appuyez sur Espace Pour continuer", centerX, centerY+20);
		ctx.fillText("Echap pour quitter", centerX, centerY+50);

		ctx.font = "100px 'Press Start 2P'";
		ctx.fillText(score, centerX, centerY+150);
		if (score > maxScore) maxScore = score;
		ctx.restore();
		document.getElementById("game").className = "noClass";
	}

	function refreshCanvas(){
		snaky.move();
		if (snaky.checkCollision()){
			gameOver();
		}
		else {
			if(snaky.isEatingApple(apple)){
				snaky.ateApple = true;
				do {
					apple.setNewPosition();
				} while(apple.isOnSnake(snaky));
			}
			ctx.clearRect(0,0,canvasWidth,canvasHeight);
			if(HardcoreModeToggle){
				CanvasStrobo();
			}
			if(score === 0 && HardcoreModeToggle === false){
				ctx.save();
				ctx.font = "20px 'Press Start 2P'";
				ctx.fillStyle = "#0F0";
				ctx.textAlign = "center";
				ctx.textBaseline = "middle";
				const centerX = canvasWidth / 2;
				const centerY = canvasHeight / 2;
				ctx.fillText("Vous pouvez activer le mode hardcore en", centerX, centerY+220);
				ctx.fillText("appuyant sur H avant de marquer un point", centerX, centerY+250);
				ctx.restore();
			}
			NewColor();
			drawScore();
			snaky.draw();
			apple.drawApple();
			timeOut = setTimeout(refreshCanvas,delay);
		}
	}

	document.onkeydown = function handlekeyDown(e){
		const key = e.key;
		console.log(key);
		let newDirection;
		switch(key){
			case "ArrowLeft" : newDirection = "left"; break;
			case "ArrowUp" : newDirection = "up"; break;
			case "ArrowRight" : newDirection = "right"; break;
			case "ArrowDown" : newDirection = "down"; break;
			case "h" : if(score === 0){
				if(HardcoreModeToggle){
					HardcoreModeToggle = false;
					canvas.style.backgroundColor = "rgba(0,255,0,0.1)";
					document.getElementById("game").className = "noClass";
				}else{
					HardcoreModeToggle = true;
					document.getElementById("game").className = "hardcore";
				}
				}
				break;
			case " " : restart(); return;
			case "Escape" :exit(); return false;
			default : return;
		}
		snaky.setDirection(newDirection);
	};

	function exit() {
		const xhr = new XMLHttpRequest();
		xhr.open('POST',window.location);
		xhr.withCredentials = true;
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.onreadystatechange = () => {
			if(xhr.readyState === 4 && xhr.status === 200) {
				location = "/game";
			}
		}
		xhr.send('userLogin=' + user + "&score=" + maxScore);
	}

	init();
};

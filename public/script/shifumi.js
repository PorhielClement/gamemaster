const selectionButtons = document.querySelectorAll('[data-selection]')
const finalColumn = document.querySelector('[data-final-column]')
const computerScoreSpan = document.querySelector('[data-computer-score]')
const yourScoreSpan = document.querySelector('[data-your-score]')
let maxScore = 0;

const SELECTIONS = [
    {
        name: 'rock',
        emoji: '✊🏿',
        beats: 'scissors'
    },
    {
        name: 'scissors',
        emoji: '🖖',
        beats: 'paper'
    },
    {
        name: 'paper',
        emoji: '✋🏻',
        beats: 'rock'
    }
]

selectionButtons.forEach(selectionButton => {
    selectionButton.addEventListener('click', e => {
        const selectionName = selectionButton.dataset.selection
        const selection = SELECTIONS.find(selection => selection.name ===selectionName)
        makeSelection(selection)
    })
})

function makeSelection(selection) {
    const computerSelection = randomSelection()
    const yourWinner = isWinner(selection, computerSelection)
    const computerWinner = isWinner(computerSelection, selection)

    addSelectionResult(computerSelection, computerWinner)
    addSelectionResult(selection, yourWinner)

    if(yourWinner) incrementScore(yourScoreSpan)
    if(computerWinner) incrementScore(computerScoreSpan)
}

function incrementScore(scoreSpan) {
    scoreSpan.innerText = parseInt(scoreSpan.innerText)+1
}

function getScore(scoreSpan) {
    return parseInt(scoreSpan.innerText);
}
function getDiffScore() {
    return getScore(yourScoreSpan) - getScore(computerScoreSpan);
}
function setMaxScore () {
    if (getDiffScore() > maxScore) maxScore = getDiffScore();
}


function addSelectionResult(selection, winner) {
    const div = document.createElement('div')
    div.innerText = selection.emoji
    div.classList.add('result-selection')
    if(winner) div.classList.add('winner')
    finalColumn.after(div)
}

function isWinner(selection, opponentSelection) {
    return selection.beats === opponentSelection.name
}

function randomSelection() {
    const randomIndex = Math.floor(Math.random() * SELECTIONS.length)
    return SELECTIONS[randomIndex]
}

document.onkeydown = function handlekeyDown(e){
    if(e.key==="Escape") {
        setMaxScore();
        exit();
        return false;
    }
}

function exit() {
    const xhr = new XMLHttpRequest();
    xhr.open('POST',window.location);
    xhr.withCredentials = true;
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4 && xhr.status === 200) {
            location = "/game";
        }
    }
    xhr.send('userLogin=' + user + "&score=" + maxScore);
}
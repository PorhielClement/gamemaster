class Game{
    /***
     * A l'appel du mot clé "new", le constructeur instancie un jeu
     * avec les informations donnés en paramètres.
     *
     * @param _id
     * @param _name
     * @param _logo
     * @param _access
     */
    constructor({_id, _name, _logo, _access}){
        this._id        = _id;
        this._name      = _name;
        this._logo      = _logo;
        this._access    = _access;
    }

    /**
     * ACCESSEURS
     */

    getId() {
        return this._id;
    }

    setId(value) {
        this._id = value;
    }

    getName() {
        return this._name;
    }

    setName(value) {
        this._name = value;
    }

    getLogo() {
        return this._logo;
    }

    setLogo(value) {
        this._logo = value;
    }

    getAccess() {
        return this._access;
    }

    setAccess(value) {
        this._access = value;
    }
}

module.exports = Game;
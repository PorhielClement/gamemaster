const Game = require('../Game');
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');

class DaoGame {
    constructor (){
        this.initDB();
    }

    /***
     * crée un lien vers le fichier où les informations seront stockées
     * et initialise la connexion avec le gestionnaire de base de données LowDB
     *
     * @returns {Promise<void>}
     */
    async initDB() {
        const adapter = new FileAsync(__dirname + '\\db.json');
        this._db = await low(adapter);
    }

    /***
     * récupère la liste des informations sur les différents jeux
     *      présents dans la base de données
     *
     * @returns {Promise<*>}
     */
    async getAllGames(){
        return this._db.get('games').value();
    }

    /***
     * Récupère les informations sur le jeu dont l'identifiant unique
     *      est envoyé en paramètres
     * @param id
     * @returns {Promise<*>}
     */
    async getGameById(id) {
        return this._db.get('games').filter({id:id});
    }

    /***
     * Renvoit le jeu dont le nom est entré en paramètre
     *
     * @param name
     * @returns {Promise<*>}
     */
    async getUserByName(name) {
        return this._db.get('games').filter({name: name});
    }

    /***
     * Créé un jeu à partir des paramètres indiqués en paramètres
     *      puis le stock dans le fichier de données
     * La promesse renvoie une erreur si celle-ci provient du stockage
     * de l'information dans la base de données.
     * Sinon, le jeu est retourné.
     *
     * @param params
     * @returns {Promise<Game>}
     */
    async createGame(params) {
        const game = new Game(params);
        await this._db.get('games').push(game).write();
        return game;
    }

    /***
     * Retrait du jeu dont les paramètres correspondent avec ceux indiqués en paramètres
     *      du fichier de données.
     *
     * @param game
     * @returns {Promise<*>}
     */
    async deleteGame(game) {
        return this._db.get('games').remove(game).write();
    }

    /***
     * Recherche du jeu "previousGame" dans le fichier de données puis remplacement
     *      des valeurs de celui-ci par celles du jeu "newGame".
     *
     * @param previousGame
     * @param newGame
     * @returns {Promise<*>}
     */
    async updateGame(previousGame, newGame){
        return this._db.get('games').find(previousGame).assign(newGame).write();
    }
}
module.exports = DaoGame;
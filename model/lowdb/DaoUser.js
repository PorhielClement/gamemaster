const User = require('../User');
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');

class DaoUser {
    constructor() {
        this.initDB();
    }

    /***
     * crée un lien vers le fichier où les informations seront stockées
     * et initialise la connexion avec le gestionnaire de base de données LowDB
     *
     * @returns {Promise<void>}
     */
    async initDB() {
        const adapter = new FileAsync(__dirname + '\\db.json');
        this._db = await low(adapter);
    }

    /***
     * récupère l'ensemble des données correspondant aux utilisateurs
     *
     * @returns {Promise<*>}
     */
    async getAllUsers() {
        return this._db.get('users');
    }

    /***
     * récupère les données de l'utilisateur dont le login est spécifié en paramètres
     *
     * @param login
     * @returns {Promise<*>}
     */
    async getUserByLogin(login) {
        return this._db
            .get('users')
            .find({_login: login})
            .value();
    }

    /***
     * récupère les données de l'utilisateur dont l'email est spécifié en paramètres
     *
     * @param email
     * @returns {Promise<*>}
     */
    async getUserByEmail(email) {
        return this._db
            .get('users')
            .find({_email: email})
            .value();
    }

    /***
     * Récupère les donnétes transmises en paramètres pour effectuer la création d'un utilisateur
     * Stockage de cet utilisateur dans la base de données et attente du résultat.
     *      - Si l'utilisateur n'est pas correctement stocké, une erreur est renvoyée.
     *      - Sinon, l'utilisateur précédemment créé est retourné
     *
     * @param params
     * @returns {Promise<User>}
     */
    async createUser(params) {
        const user = new User(params);
        await this._db
            .get('users')
            .push(user)
            .write();
        return user;
    }

    /***
     * Retrait du user spécifié dans le fichier de base de données
     * Les couples paramètre - valeur de l'utilisateur spécifié constituent
     *      les conditions de recherche
     *
     * @param user
     * @returns {Promise<*>}
     */
    async removeUser(user) {
        return this._db
            .get('users')
            .remove(user)
            .write();
    }

    /***
     * Retrait de tous les utilisateurs de la base de données
     * L'objet vide correspond à une condition universelle
     * La fonction write permet d'effectuer l'opération
     *
     * @returns {Promise<*>}
     */
    async removeAllUsers() {
        return this._db
            .get('users')
            .remove({})
            .write();
    }

    /***
     * Trouve le "previousUser" dans la liste des utilisateurs
     *      puis remplace ses valeurs par celles du newUser
     *
     * @param previousUser
     * @param newUser
     * @returns {Promise<*>}
     */
    async updateUser(previousUser, newUser) {
        return this._db
            .get('users')
            .find(previousUser)
            .assign(newUser)
            .write();
    }

    /***
     * Indique si la liste d'utilisateur ayant le login indiqué est vide ou non.
     * Donc indique la présence de l'utilisateur dans la base de données par son login
     *
     * @param login
     * @returns {Promise<boolean>}
     */
    async existsUserByLogin(login) {
        return this.getUserByLogin(login)
            .size()
            .value() > 0;
    }

    /***
     * Indique la présence d'un utilisateur en prenant son email comme paramètre de recherche
     *
     * @param email
     * @returns {Promise<boolean>}
     */
    async existsUserByEmail(email) {
        return this.getUserByEmail(email)
            .size()
            .value() > 0;
    }

    /***
     * Recherche la présence de l'utilisateur par son login.
     *      - S'il est trouvé, l'utilisateur est renvoyé
     *      - Sinon, une recherche est lancée avec son email comme paramètre de recherche
     *          - S'il est trouvé, l'utilisateur est renvoyé
     *          - Sinon, indique que l'utilisateur n'est pas présent dans la base de données
     *
     * @param login
     * @param email
     * @returns {Promise<boolean>}
     */
    async existsUser(login, email) {
        return this.getUserByLogin(login)
            .then((result)=>
            !!result ? true : this.getUserByEmail(email)
                //true  : user exists by login
                //false : doesn't exists by login. try again with email
            ).then((result)=>
            !!result
                //true  : user exists by email. user>undefined>user
                //false : user doesn't exists by email. undefined>true>false
            );
    };

    /***
     * Récupère l'utilisateur spécifié par son login puis compare son mot de passe avec celui entré en paramètres
     *      - Si les mots de passes sont identiques, l'utilisateur est renvoyé
     *      - Sinon est indiqué la non correspondance du login avec le mot de passe
     *
     * @param login
     * @param mdp
     * @returns {Promise<*>}
     */
    async checkLogin(login,mdp) {
        return this.getUserByLogin(login)
            .then((result) => {
                return result && result._pwd === mdp
                    ? new User(result)
                    : false
            });
    }
    // return (this.getUserByLogin(login).size().value() > 0 || this.getUserByEmail(email).size().value() > 0);
}

module.exports = DaoUser;
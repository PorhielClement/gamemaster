/**
 * Permet au reste de l'application d'établir simplement la connexion
 * avec tous les gestionnaires de classe prenant en charge le
 * système de base de données LowDB
 */
module.exports.DaoUser  = require('./DaoUser');
module.exports.DaoGame  = require('./DaoGame');
module.exports.DaoScore = require('./DaoScore');
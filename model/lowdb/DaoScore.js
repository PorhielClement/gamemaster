const Score = require('../Score');
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');

class DaoScore {
    constructor() {
        this.initDB();
    }

    /***
     * crée un lien vers le fichier où les informations seront stockées
     * et initialise la connexion avec le gestionnaire de base de données LowDB
     *
     * @returns {Promise<void>}
     */
    async initDB() {
        const adapter = new FileAsync(__dirname + '\\db.json');
        this._db = await low(adapter);
    }

    /***
     * Récupère tous les scores présents dans le fichier de données
     *
     * @returns {Promise<*>}
     */
    async getAllScores() {
        return this._db
            .get('scores')
            .sortBy('_points','asc');
        /** tri des résultats par ordre croissants*/
    }

    /***
     * Récupère le score obtenu par un joueur donné sur un jeu donnée.
     *
     * @param userLogin
     * @param gameId
     * @returns {Promise<*>}
     */
    async getScoreByUserByGame(userLogin, gameId) {
        return this._db
            .get('scores')
            .find({
                _userLogin: userLogin,
                /** Condition 1 : le score recherché doit correspondre
                 * à un score de l'utilisateur indiqué */
                _gameId: gameId
                /** Condition 2 : le score recherché doit correspondre
                 * au jeu indiqué */
            })/** la fonction find() renvoit le premier score trouvé
              validant toutes les conditions*/
            .value();
    }

    /***
     * Récupère tous les scores d'un utilisateur donné en paramètres
     *
     * @param userLogin
     * @returns {Promise<*>}
     */
    async getScoresByUser(userLogin) {
        return this._db
            .get('scores')
            .filter({_userLogin: userLogin})
            .value();
    }

    /***
     * Récupère le meilleur score de chaque utilisateur pour un jeu donné
     *
     * @param gameId
     * @returns {Promise<*>}
     */
    async getScoresByGame(gameId) {
        return this._db
            .get('scores')
            .filter({_gameId: gameId})
            .sortBy('_points')
            .value();
    }

    /***
     * Récupère le score et son jeu associé
     *      où le joueur indiqué a le meilleur score
     *
     * @param userLogin
     * @returns {Promise<*>}
     */
    async getBestScoreByUser(userLogin) {
        return this._db.get('scores')
            .filter({_userLogin: userLogin})
            .sortBy('points')
            .take(1)
            .value()
    }

    /***
     * Récupère le top 5 des scores et par conséquent le nom des joueurs
     *      ayant atteint les meilleurs scores pour le jeu indiqué
     *
     * @param gameId
     * @returns {Promise<*>}
     */
    async getBestScoresByGame(gameId) {
        return this._db
            .get('scores')
            .filter({_gameId: gameId})
            /** Récupère tous les scores du jeu indiqué */
            .sortBy('points')
            /** Tris tous les résultats */
            .take(5)
            /** Pour ne retourner que les 5 premiers*/
            .value();
    }

    /***
     * Crée une instance de la classe Score avec les paramètres donnés
     *      puis stoque ce score dans le fichier de données
     *
     * @param params
     * @returns {Promise<boolean>}
     */
    async createScore(params) {
        const score = new Score(params);
        await this._db
            .get('scores')
            .push(score)
            .write();
        return true;
    }

    /***
     * Recherche le score d'un joueur à un jeu pour lui assigner
     *      un score plus important (configuré directement dans les jeux)
     *
     * @param oldScore
     * @param newScore
     * @returns {Promise<*>}
     */
    async updateScore(oldScore, newScore) {
        const score = new Score(newScore);
        return this._db
            .get('scores')
            .find(oldScore)
            .assign(score)
            .write();
    }

    /***
     * Indique si un score correspond au couple joueur - jeu
     *
     * @param userLogin
     * @param gameId
     * @returns {Promise<boolean>}
     */
    async existsScore(userLogin, gameId) {
        return this.getScoreByUserByGame()
            .then((result) =>
            !!result
        );
    }
}

module.exports = DaoScore;
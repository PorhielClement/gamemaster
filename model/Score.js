class Score {
    /***
     * A l'appel du mot clé "new", le constructeur instancie un score
     * avec les informations donnés en paramètres.
     *
     * Ce score représente le meilleurs score obtenu par un joueur à un jeu
     *
     * @param _userLogin
     * @param _gameId
     * @param _points
     */
    constructor ({_userLogin, _gameId, _points}) {
        this._userLogin = _userLogin;
        this._gameId    = _gameId;
        this._points    = _points;
    }

    /**
     * ACCESSEURS
     */

    getUserLogin() {
        return this._userLogin;
    }

    setUserLogin(value) {
        this._userLogin = value;
    }

    getGameId() {
        return this._gameId;
    }

    setGameId(value) {
        this._gameId = value;
    }

    getPoints() {
        return this._points;
    }

    setPoints(value) {
        this._points = value;
    }


}
module.exports = Score;
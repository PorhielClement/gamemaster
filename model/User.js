class User{
    /***
     * A l'appel du mot clé "new", le constructeur instancie un utilisateur
     * avec les informations donnés en paramètres.
     *
     * @param _name
     * @param _firstname
     * @param _login
     * @param _pwd
     * @param _email
     */
    constructor({_name, _firstname, _login, _pwd, _email}) {
        this._name      = _name;
        this._firstname = _firstname;
        this._login     = _login;
        this._pwd       = _pwd;
        this._email     = _email;
    }

    /**
     * ACCESSEURS
     */

    getName() {
        return this._name;
    }

    setName(value) {
        this._name = value;
    }

    getFirstname() {
        return this._firstname;
    }

    setFirstname(value) {
        this._firstname = value;
    }

    getLogin() {
        return this._login;
    }

    setLogin(value) {
        this._login = value;
    }

    getPwd() {
        return this._pwd;
    }

    setPwd(value) {
        this._pwd = value;
    }

    getEmail() {
        return this._email;
    }

    setEmail(value) {
        this._email = value;
    }
}

module.exports = User;
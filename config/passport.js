/* import des stratégies d'authentification */
const LocalStrategy = require('passport-local').Strategy;
const GithubStrategy = require('passport-github').Strategy;
/*********************************************/
const db = require('../model/lowdb/');  // fonctions pour la base de donnée (ici, lowdb)
const daoUser = new db.DaoUser();       // fonction DAO pour la table User

/* configuration des stratégies d'authentification */
module.exports = (passport) => {                // passage de la configuration du module principal de passport
    passport.serializeUser((user, done)=> {     // sérialisation de l'utilisateur
        done(null, user._login);                // renvoi du login
    });

    passport.deserializeUser((login, done)=>{   // désérialisation de l'utilisateur
        daoUser.getUserByLogin(login)           // cherche un utilisateur en fonction de son login
            .then((user) => {
                if(user)
                    done(null, user);               // renvoi de l'objet utilisateur si trouvé
                else
                    done(true, false);          // renvoi d'une erreur vide sinon
            })
            .catch((err) => {                   // en cas d'erreur
                done(err, null);                // renvoi de l'erreur (sans l'objet utilisateur)
            });
    });

    /* configuration de la stratégie locale d'enregistrement */
    passport.use('local-signin', new LocalStrategy({
            usernameField: '_email',                    // le champ _email de la requête sera le champ utilisé pour l'identifiant de l'utilisateur
            passwordField: '_pwd',                      // le champ _pwd de la requête sera le champ utilisé pour le mot de passe de l'utilisateur
            passReqToCallback: true                     // pour renvoyer l'objet Request dans la fonction suivante
        },
        (req, _email, _pwd, done) => {                  // fonction appliquée à chaque demande d'enregistrement
            process.nextTick(() => {                    // fonction de temporisation
                daoUser.existsUser(req.body._login,req.body._email)                                         // cherche l'utilisateur selon son login et email
                    .then((user) => {
                        if(user)                                                                                // échec si l'utilisateur existe
                            done(null, false, req.flash('errorMessage', 'Adresse email ou login déjà utilisé'));// renvoi d'un message à usage unique
                        else if(!_pwd)                                                                          // échec si aucun mot de passe renseigné
                            done(null, false, req.flash('errorMessage', 'Mot de passe non valide'));
                        else if(!req.body._name)                                                                // échec si aucun nom renseigné
                            done(null, false, req.flash('errorMessage', 'Veuillez indiquer votre nom'));
                        else {
                            daoUser.createUser(req.body).then((newUser) => {                                    // création d'un nouvel utilisateur
                                if(newUser)                                                                     // si retour sans erreur
                                    // renvoi de l'utilisateur avec un message de succès
                                    done(null, newUser, req.flash('successMessage', 'Bienvenue, ' + newUser._name + ' !'));
                                else
                                    done(null, false, req.flash('errorMessage', 'Erreur interne. Veuillez réessayer ultérieurement'));
                            });
                        }
                    }).catch((err) => {                                                                         // en cas d'erreur dans toutes les fonctions précédentes
                    // renvoi d'une erreur à la vue
                    done(err, false, req.flash('errorMessage', 'Erreur interne. Veuillez réessayer ultérieurement'));
                });
            });
        }));


    /* configuration de la stratégie locale de connexion */
    passport.use('local-login', new LocalStrategy({
            usernameField: '_login',
            passwordField: '_pwd',
            passReqToCallback: true
        },
        (req, _login, _pwd, done) => {
            process.nextTick(() => {
                daoUser.checkLogin(_login, _pwd)
                    .then((user) => {
                        if(!user)
                            done(null, false, req.flash('errorMessage', 'Adresse email ou mot de passe invalide'));
                        else
                            done(null, user, req.flash('successMessage', 'Bonjour, ' + user._name + ' !'));
                    }).catch((err) => {
                    done(err, false, req.flash('errorMessage', 'Erreur interne. Veuillez réessayer ultérieurement'));
                });
            });
        }));


    /* configuration de la stratégie d'authentification via GitHub */
    passport.use('github', new GithubStrategy({
            clientID: 'fff01b0367bde875bf0c',                           // identifiant du serveur auprès de l'authentification GitHub
            clientSecret: '5872e9fead6ff8f3e6bc1072e110edf0e49ecc0b',   // mot de passe du serveur
            callbackURL: 'http://localhost/auth/github/callback',       // adresse de redirection (réception du code d'authentification)
            profileFields: ['id', 'name', 'email','photos'],            // champs à récupérer en cas de succès
            passReqToCallback: true
        },
        (req, accessToken, refreshToken, profile, done) => {
            process.nextTick(() => {
                daoUser.getUserByLogin(profile.username)                // recherche en fonction du nom du compte GitHub de l'utilisateur
                    .then((user) => {
                        if(user)
                            done(null, user, req.flash('successMessage', 'Bonjour, ' + user._firstname + ' ' + user._name + ' !'));
                        else {
                            const splittedName = profile.displayName.split(' ');    // séparation du nom et prénom
                            const newUser = {};                                     // objet pour la création de l'utilisateur
                            newUser._login = profile.username;
                            newUser._firstname = splittedName[0];
                            newUser._name = splittedName[splittedName.length - 1];
                            newUser._email = profile.emails[0].value;               // récupération de la première adresse mail possible

                            daoUser.createUser(newUser)
                                .then((user) => {
                                    done(null, user, req.flash('successMessage', 'Bienvenue, ' + user._firstname + ' ' + user._name + ' !'));
                                })
                                .catch((err) => {
                                    done(err, false,  req.flash('errorMessage', 'Erreur interne. Veuillez réessayer ultérieurement'));
                                });
                        }
                    }).catch((err) => {
                    if(err)
                        done(err, false,  req.flash('errorMessage', 'Erreur interne. Veuillez réessayer ultérieurement'));
                });
            });
        }));
};

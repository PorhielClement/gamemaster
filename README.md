# [gamemaster](https://gitlab.com/PorhielClement/gamemaster/)

## Introduction

Projet de plateforme proposant des jeux en ligne et un tableau des scores

Réalisé par Clément Porhiel ([Sicario](https://gitlab.com/PorhielClement)) et Dorian Souleyreau ([DodoDye](https://github.com/DodoDye/))

## Utilisation

Pour installer le projet :

    git clone https://gitlab.com/PorhielClement/gamemaster/
    cd gamemaster
    npm install
    
Lancer le serveur :

    node server.js

## Liens

[Vidéo de présentation](https://we.tl/t-YV4isnQcMy)

[Utilisation de parties d'un projet existant](https://github.com/DodoDye/PortfolioV2)

## Points d'améliorations

 - Corriger la déconnexion intempestive
 - Faire fonctionner les accesseurs (objets User, Game, Score)
 - Ajouter des statistiques pour les scores
 - Créer un mode admin
 - Créer une page d'admin pour bannir des joueurs
 - Améliorer le style
 - Homogénéiser la syntaxe et les commentaires

